BASE_DOCKER_IMAGE = debian:bullseye

default:
	@cat Makefile

shell_darwin_x86_64:
	@support/darwin_shell.sh x86_64

shell_darwin_arm64:
	@support/darwin_shell.sh arm64

shell_linux: docker-build
	@docker run --rm -ti -e CLEAN_ENV=1 -e DOCKER_ENV=1 -v $(shell pwd):/app -w /app asdf-prebuilt-package-builder bash -c 'support/linux_shell.sh'

build:
	@./support/build.sh

create_tar_balls:
	@./support/create_tar_balls.sh

docker-build:
	@docker pull ${BASE_DOCKER_IMAGE}
	docker build --build-arg BASE_DOCKER_IMAGE=${BASE_DOCKER_IMAGE} -t asdf-prebuilt-package-builder .
