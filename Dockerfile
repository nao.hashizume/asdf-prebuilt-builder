ARG BASE_DOCKER_IMAGE
FROM ${BASE_DOCKER_IMAGE}

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Etc/UTC

RUN apt-get update && apt-get install -y build-essential git tmux vim curl libreadline-dev libssl-dev libssl-dev uuid-dev zlib1g-dev sudo pkg-config lsb-release icu-devtools
RUN useradd -m -G sudo -p "$(openssl passwd -1 gdk)" gdk

USER gdk
