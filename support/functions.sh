source "$(cd "$(dirname "$0")" || exit ; /bin/pwd -L)/basic_functions.sh"

start_clean_darwin_build_shell() {
	local arch_forced="${1}"
	local brew_prefix

	if [[ "${arch_forced}" == "arm64" ]]; then
		brew_prefix="/opt/homebrew"
	else
		brew_prefix="/usr/local"
	fi

	arch -${arch_forced} env -i PWD=${PWD} USER=$(whoami) HOME=${HOME} TERM=${TERM} CLEAN_ENV=1 ARCH_FORCED=${arch_forced} BREW_PREFIX=${brew_prefix} bash --init-file .gdkrc_build
}

start_clean_linux_build_shell() {
	local arch_forced="${1}"

	env -i USER=$(whoami) PWD=${PWD} HOME=${HOME} TERM=${TERM} CLEAN_ENV=1 ARCH_FORCED=${arch_forced} bash --init-file .gdkrc_build
}
