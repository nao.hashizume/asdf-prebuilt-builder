#!/bin/bash

set -euo pipefail

ARCH_FORCED="${1}"

if [[ ! -z "${CLEAN_ENV:-}" ]]; then
  echo "ERROR: We're already in a clean env!" >&2
  exit 1
fi

if [[ -z "${ARCH_FORCED:-}" ]]; then
  echo "ERROR: Architecture to compile as needs to be provided, e.g. arm64 or x86_64"  >&2
  exit 1
fi

source "$(cd "$(dirname "$0")" || exit ; /bin/pwd -L)/functions.sh"

if is_os_darwin; then
	if is_arch_supported ${ARCH_FORCED}; then
		start_clean_darwin_build_shell ${ARCH_FORCED}
  else
    echo "ERROR: Unable to run in '${ARCH_FORCED}' mode." >&2
		exit 1
  fi
else
  echo "ERROR: Executing environment needs to be macOS." >&2
  exit 1
fi
